# README #

Nodestat is a bash script for checking the CPU usage of many nodes of a cluster quickly.

### Usage ###

Without installing, the script can be used by passing hostnames on the command line:

```
#!bash
bash nodestat.sh node001 node002 ...
```
or more compactly using shell expansion,
```
#!bash
bash nodestat.sh node{001..020}
```
For node001 through node020, for example.

### Installation ###

If you want to make nodestat a command for your shell, place it somewhere safe and create an alias in your ~/.bashrc containing the hostnames you wish to query:
```
#!bash
alias nodestat='bash /path/to/nodestat.sh node001 node002 ...'
```
And don't forget to
```
#!bash
source ~/.bashrc
```