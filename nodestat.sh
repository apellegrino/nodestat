#!/bin/bash

# Andrew Pellegrino
# April 2017
#
# Get the CPU usage of many machines over SSH whose hostnames are passed
# in on the command line. If none are provided, defaults are used.
# 
# It is recommended to only query machines to which the user can login without
# a password, and usernames are the same between sending and receiving machines.

if [[ $# -eq 0 ]] ; then
    echo "Usage: nodestat <hostname01> [hostname02] [hostname03]..."
    exit 1
fi
NODE_LIST=$@

FORMAT="s"
TEMP_FILE=$(mktemp)

function grab_usage() {
    CPU_PERCENT=$(ssh $1 "top -bn2 | head -n 107 | tail -n 100 | awk 'BEGIN {usage=0} {usage+=\$9} END {print usage}'")
    if [[ $? -ne 0 ]] ; then
        printf "$1 FAILED\n" >> $TEMP_FILE
        return 1
    fi

    NPROCS=$(ssh $1 "grep processor /proc/cpuinfo | wc -l")
    NTICKS=$(echo "($CPU_PERCENT+50)/100" | bc)
    NSPACES=$(echo "$NPROCS-$NTICKS" | bc)

    A=$(printf "$1 [")
    B=$(printf "%-$NTICKS$FORMAT" | sed "s/\ /#/g")
    C=$(printf "%-$NSPACES$FORMAT")
    D=$(printf "] ($NTICKS/$NPROCS)")

    printf "$A$B$C$D\n" >> $TEMP_FILE
}


for node in $NODE_LIST; do
    grab_usage "$node" &
done

wait

sort $TEMP_FILE
rm $TEMP_FILE
